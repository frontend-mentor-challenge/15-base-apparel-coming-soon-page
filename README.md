# Frontend Mentor - Base Apparel coming soon page solution

This is a solution to the [Base Apparel coming soon page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/base-apparel-coming-soon-page-5d46b47f8db8a7063f9331a0). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page
- Receive an error message when the `form` is submitted if:
  - The `input` field is empty
  - The email address is not formatted correctly

### Screenshot

![Mobile](./screenshots/mobile.jpg)
![Desktop](./screenshots/desktop.jpg)

### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/15-base-apparel-coming-soon-page)
- Live Site URL: [Live](https://base-apparel-coming-soon-page-et59.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
- [Google Fonts](https://fonts.google.com/)
- [SASS](https://sass-lang.com/) - SASS
- [Formik](https://formik.org/) - Forms

### What I learned

### Continued development

### Useful resources

- [Figma](https://figma.com) - For design
- [React](https://es.react.dev/reference/react) - For resolving doubts

## Author

- Website - [Issac Leyva](In construction)
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments

