import './App.scss'
import Logo from './assets/images/logo.svg'

import EmailForm from './components/EmailForm/EmailForm'

function App() {

  return (
    <main className="coming-soon">
      <header className="coming-soon__header">
        <img src={Logo} alt="" />
      </header>
      <div className="coming-soon__hero"></div>
      <section className="coming-soon__content">
        <h1 className="coming-soon__title"><span>We&apos;re </span>coming soon</h1>
        <p className="coming-soon__text">Hello fellow shoppers! We&apos;re currently building our new fashion store. Add your
          email below to stay up-to-date with announcements and our launch deals.</p>
          <EmailForm/>
      </section>

    </main>
  )
}

export default App
