import { Formik } from 'formik'
import './EmailForm.scss'
import iconArrow from '../../assets/images/icon-arrow.svg'

const EmailForm = () => {
    return (
        <Formik
            initialValues={{email: ''}}
            validate={values => {
                const errors = {}
                console.log(errors)
                if (!values.email) {
                    errors.email = 'The email field is empty'
                } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
                    errors.email = 'The email address is not formatted correctly'
                }
                return errors
            }}
            onSubmit={(values, {setSubmitting}) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    setSubmitting(false);
                }, 400);
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
            }) => (
                <form className="coming-soon__form" onSubmit={handleSubmit}>
                    <div className="coming-soon__field">
                        <label htmlFor="email">Email Address</label>
                        <input
                            type='email'
                            name='email'
                            // className='form__input'
                            className={`coming-soon__input ${errors.email ? 'coming-soon__input--error' : ''}`}
                            placeholder='Email Address'
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                        />
                        <span className="coming-soon__message">{errors.email && touched.email && errors.email}</span>
                        <button type="submit" className="coming-soon__submit" disabled={isSubmitting}>
                            <img src={iconArrow} alt="" />
                        </button>
                    </div>
                </form>
            )}
        </Formik>
    )
}

export default EmailForm